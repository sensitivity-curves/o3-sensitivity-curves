# O3 Sensitivity Curves

Files with .txt extension are two-column frequency, ASD ASCII files.

Files with .ecsv extension have ECSV comments lines in the file headers.

## L1 curves with no cleaning or subtraction:
`O3-L1-C01-1240573680.0_sensitivity_strain_asd.txt`

This file is from https://dcc.ligo.org/LIGO-G1902348.

"These are calibrated strain and displacement spectra from a representative best
of O3a, taken on Apr 29 2019.

----

`O3-L1-C01-1262141640.0_sensitivity_strain_asd.txt'

This file is from https://dcc.ligo.org/LIGO-G2001365

"These are calibrated strain and displacement spectra from a representative best
of O3b, taken on Jan 04 2020.

----

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C01," DCS frames
in LIGO jargon). We expect the uncertainty in calibration at this time 5% and 3
deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is larger and
unquantified outside this band).

The code that produced this plot can be found via T1500365"

***

## L1 curves with cleaning and subtraction:
`O3-L1-C01_CLEAN_SUB60HZ-1240573680.0_sensitivity_strain_asd`

This file is from https://dcc.ligo.org/LIGO-G1902347

These are calibrated strain and displacement spectra from a representative best
of O3a, taken on Apr 29 2019.

----

`O3-L1-C01_CLEAN_SUB60HZ-1262141640.0_sensitivity_strain_asd.txt'

This file is from https://dcc.ligo.org/LIGO-G2001365

These are calibrated strain and displacement spectra from a representative best
of O3b, taken on Jan 04 2020.

----


This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C01," DCS frames
in LIGO jargon). The data has been processed to subtract the linear noise
contributions from calibration lines and 60 Hz line. Further, the linear,
non-stationary noise around the 60 Hz line has been subtracted using the
NONSense method (P1900335). We expect the uncertainty in calibration at this
time 5% and 3 deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is
larger and unquantified outside this band).

The code that produced this plot can be found via T1500365

Cleaning documented in *Phys. Rev. D 101, 042003 (2020)*

***

## H1 curve with no cleaning or subtraction:
`O3-H1-C01-1251752040.0_sensitivity_strain_asd.txt`

This file is from https://dcc.ligo.org/LIGO-G1902349

These are calibrated strain spectra from a representative best of O3a, taken on
Sep 05 2019.

----

`O3-H1-C01-1262197260.0_sensitivity_strain_asd.txt'

This file is from https://dcc.ligo.org/LIGO-G2001364

These are calibrated strain and displacement spectra from a representative best
of O3b, taken on Jan 04 2020.

----

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C01," DCS frames
in LIGO jargon). We expect the uncertainty in calibration at this time 5% and 3
deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is larger and
unquantified outside this band).

The code that produced this plot can be found via T1500365

***

## H1 curve with cleaning and subtraction:
`O3-H1-C01_CLEAN_SUB60HZ-1251752040.0_sensitivity_strain_asd.txt`

These are calibrated strain and displacement spectra from a representative best
of O3a, taken on Sep 05 2019.

----
`O3-H1-C01_CLEAN_SUB60HZ-1262197260.0_sensitivity_strain_asd.txt'

This file is from https://dcc.ligo.org/LIGO-G2100674

These are calibrated strain and displacement spectra from a representative best
of O3b, taken on Jan 04 2020.

----

This data is from that regenerated offline to account for data dropouts and has
been compensated for time-dependence in the detector response ("C01," DCS frames
in LIGO jargon). The data has been processed to subtract the linear noise
contributions from calibration lines and 60 Hz line. Further, the linear,
non-stationary noise around the 60 Hz line has been subtracted using the
NONSense method (P1900335). We expect the uncertainty in calibration at this
time 5% and 3 deg [deg], valid between 10 and 2000 [Hz] (the uncertainty is
larger and unquantified outside this band).

The code that produced this plot can be found via T1500365

Cleaning documented in *Phys. Rev. D 101, 042003 (2020)*

***
